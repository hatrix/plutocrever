Title: Détection des caractères, 2e partie
Date: 2014-10-07 18:40:00
Category: News
Tags: détection, caractères, blocs
Author: hatrix

On avait donc des caractères de détectés, mais pas vraiment proprement ! On
avait en effet quelques fois du blanc en haut ou en bas, il faut le virer.

On commence par le haut : dès la première ligne contenant des pixels, BAM, on
change la position en y du caractère ainsi que sa hauteur. Ce qui nous donne
cette image :

![Détection des caractères](../images/detection_c2.png)

Il reste encore le bas à faire. Même principe mais inversé : on change la
hauteur dès qu'on trouve une ligne vide. Ce qui nous donne cette fois ci bien
ce qu'on cherche :

![Détection des caractères](../images/detection_c3.png)

Il reste néanmoins un petit problème à régler. Certains caractères, notamment
le « r » du mot « j'aimerais » qui est coupé un pixel trop tard...
