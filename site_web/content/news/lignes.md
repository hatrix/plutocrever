Title: Détection des lignes
Date: 2014-10-06
Category: News
Tags: détection, lignes, blocs
Author: hatrix

Jusqu'à maintenant, tout était plutôt simple. Je viens de terminer une 
reconnaissance naïve des lignes. J'avais avant ça aussi codé une détection de
l'angle qui marche vraiment pas mal. J'en parlerai dans un autre article.

Donc, cette détection des lignes... On part tout d'abord d'une image TRÈS
propre : pas d'artéfacts, caractères bien distincts, tout est joli.
Voici l'image de départ :

![Avant traitement](../images/detection_lignes.png)

Pour trouver les lignes, il faut parcourir l'histogramme vertical de l'image.
Si l'on rencontre une valeur autre que 0, et donc des pixels noirs sur la
ligne, on assume que c'est une partie d'une ligne de texte, l'image étant
parfaite. J'ai décidé d'utiliser un tableau de SDL_Rect pour contenir les
coordonnées des lignes, on aurait pu se passer des composantes x et width mais
c'est pas plus mal, on pourra se reservir des fonctions plus tard. Et puis là
en fait je doute, ça aurait pas été mieux de faire un SDL_Point avec x=height
et y=y ?

Bref, après avoir fait tout ça, on se retrouve avec un tableau de SDL_Rect. On
peut l'afficher, on peut faire joujou avec mais surtout, on peut afficher les
lignes détectées à l'écran !

Voici donc les lignes détectées sur l'exemple précédent :

![Après traitement](../images/detection_lignes2.png)

On voit bien que les lignes ont été correctements détectées. La prochaine
étape est la détection des caractères. Étant donné qu'on a une image parfaite,
ça risque de se passer exactement comme pour les lignes mais avec un 
histogramme horizontal !

Après ça il faudra encore peaufiner les algos de binarisation et de suppression
du bruit. Tel quel, ça sert à rien, on reconnait des lignes alors qu'il n'y a
que des pixels qui traînent au pif. Après on peut se dire que c'est le rôle
de la détection des lignes de gérer ce merdier. J'pense à peut-être faire une
moyenne de la hauteur de chaque ligne, et, si on a une hauteur vraiment trop
petite (3 ou 4 pixels qui traînent par exemple), eh bien on ne la compte pas.

Allez, au boulot ! Après l'essay d'anglais et d'autres choses passionantes...
