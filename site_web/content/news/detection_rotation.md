Title: Détection de l'angle de rotation
Date: 2014-10-09
Category: News
Tags: détection, angle, rotation
Author: hatrix

Certaines images à traiter peuvent ne pas avoir été scannées correctement. On
peut en effet avoir un angle anormal par rapport à la verticale habituelle.
Le réseau de neuronnes ne va pas détecter des caractères sous plusieurs angles,
faut pas déconner non plus. Du coup on va détecter cet angle puis effectuer une
rotation.

Prennons l'image ci-dessous comme example :

![Avant rotation](../images/rotation_avant.png)

Cette image a un angle de 13 degrés. Pour pouvoir la lire, une méthode très
simple nous a été proposée : la skew detection.
Celle si se base sur des histogrammes ainsi que la variance de celle-ci. Faire
un histograme, c'est facile. Avec un angle, un peu plus dur mais ce n'est
qu'une tangente et une petite multiplication.
Il y a toutefois quelques points auxquels il faut faire attention, notamment
lorsqu'on trace des lignes dans les coins de l'image. Une ligne qui part d'une
extremité mais n'atteint pas l'autre ne vaut rien. Elle n'aura pas parcouru
toute la largeur de l'image et son nombre de pixels sera faussé.

L'image ci-dessous représente une détection mal faite qui résultera en un
histogramme faussé et donc un angle trouvé faux :

![Fausse détection](../images/detection_angle_fausse.png)

Cette fois-ci, la même image mais avec un histogramme correctement tracé :

![Fausse détection](../images/detection_angle_juste.png)

Je vous renvoie maintenant à vos cours de lycée pour la variance. Ou si vous
avez la flemme, voici ce que ça fait concrètement : la variance sert à 
déterminer à quel point les valeurs d'un ensemble sont éparpillées. Par 
exemple, la liste [0, 0, 0, 20, 20, 20] aura une moyenne de 10 mais sa variance
sera beaucoup plus élevée que pour une liste [10, 10, 10, 10, 10, 10] ayant la
même moyenne.

Le principe est qu'avec un angle correct, on rencontrera forcément très peu de
pixels lors d'interlignes et forcément plus lorsqu'on est sur une ligne de
texte.
Les mauvais angles auront une variance moins forte, étant donné qu'il y aura
moins de lignes blanches dans l'histogramme. On prend ainsi l'histogramme de
l'angle ayant la variance la plus élevée, on met un petit signe moins devant,
et c'est gagné !

Il ne reste plus qu'à effectuer une rotation avec l'angle trouvé, ce qui nous
donne :

![Avant rotation](../images/rotation_apres.png)
