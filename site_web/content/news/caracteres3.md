Title: Détection des caractères, ça rate !
Date: 2014-10-11
Category: News
Tags: détection, caractères, blocs
Author: hatrix

On avait vu que la détection des caractères marchait à peu près. Hormis 
quelques problèmes de seuil pour des caractères un peu proches, ça marchait pas
trop mal.

J'ai alors décidé de tester un scan disponible sur 
[wiki-prog](http://infoprepa.epita.fr/ImagesOCR/), la UneColUneFont300 parce
que c'est la plus simple, avec une résolution pas trop mauvaise. On commence
donc par la binariser, je l'ai fait via imagemagick parce que c'était plus
rapide, on verra quand j'aurai un bon algo de Sauvola.

On part ainsi de cette image :

![UneColUneFont](../images/scanMarwan.jpg)

Et après une détection des caractères :

![UneColUneFont](../images/scanMarwanResult.png)

On voit bien qu'il y a plusieurs problèmes : le cadre en haut n'est pas 
vraiment un caractère, le « L » fait chier. Ensuite, toujours ce problème de
seuil car certains caractères sont reconnus deux à deux. On peut quand même
remarquer qu'à la première ligne de l'avant dernier paragraphe, le mot
« 200ème » marche vraiment pas mal !
On a encore du bruit qu'il faut virer, faut voir avec l'algo de Sauvola ce que
ça donne.
