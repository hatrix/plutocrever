Title: Niveaux de gris
Date: 2014-09-14
Category: News
Tags: gris, image, pré-traitement
Author: hatrix

Durant le pré-traitement de l'image, c'est à dire avant d'analyser vraiment
l'image, il faut effectuer quelques modifications de celle-ci afin de la rendre
plus lisible.
La première étape de ce pré-traitement est de passer en niveaux de gris 
l'image.

Cette étape est plutôt facile. Après avoir dû coder getPixel et setPixel, il ne
restait plus qu'à obtenir les valeurs RGB du pixel traité pour en obtenir un
niveau de gris. Pour ce faire, la composante **r** du pixel est multipliée par
0.3, **g** par 0.59 et **b** par 0.11. En réunissant toutes ces composantes en
un pixel, on obtient un gris mieux réussi que si l'on avait utilisé une moyenne
des valeurs r, g et b.

C'est en codant cette fonction que j'ai remarqué que la SDL en C, c'est louche.
Il y'a une fonction qui donne un donne un pixel sous forme d'Uint32, étant 
donnée le format de la surface et les composantes RGB. C'est pas mal haut 
niveau quand on pense qu'il faut recoder getPixel et setPixel soi-même pour
gérer les différents formats d'image. Plus impréssionant encore, il y a une 
fonction qui effectue la rotation d'une surface, wtf ?
