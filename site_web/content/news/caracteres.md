Title: Détection des caractères
Date: 2014-10-07 12:00:00
Category: News
Tags: détection, caractères, blocs
Author: hatrix

Après avoir détecté les lignes, il faut bien évidemment détecter les 
caractères ! C'est un poil plus compliqué que pour les lignes, mais rien de
méchant, vraiment.

Étant donné qu'on a un tableau contenant les coordonnées des lignes, on va pas
se priver pour le parcourir. Après ça, il faut faire un histograme horizontal
en parcourant chaque ligne.
Comme pour les lignes, si la colonne actuelle est noire est celle d'avant
blanche, on a un début de caractère. Pareil pour la fin avec noire puis 
blanche.

À la fin, on retourne encore une fois un tableau de SDL_Rect, ce qui a vraiment
du sens ici vu qu'un caractère n'a pas forcément la même largeur et peut être
n'importe où dans le texte.

Pour reprendre l'image précédente, voici ce qu'on obtient après une détection
des lignes, puis des caractères :

![Détection des caractères](../images/detection_caracteres.png)

On voit ici que certains caractères ne sont pas correctement entourés. Certains
ont en effet encore du blanc en haut ou en bas. Vu qu'on a détecté des lignes,
on a récupéré les bornes hautes et basses de celles-ci, et donc des caractères
comme des majuscules, « j » ou encore « p » font bien varier cette hauteur de
ligne.
Il faut donc relancer un genre de détection de ligne, mais sur chaque 
caractère. Je dis bien genre, parce qu'en fait, on ne va pas créer de nouveaux
caractères, on veut juste virer le blanc en haut et en bas. On va juste faire
une boucle qui modifie la position quand elle rencontre le premier pixel noir
et quand elle rencontre le dernier.

