Title: Détection des lignes, 2e partie
Date: 2014-10-12
Category: News
Tags: détection, lignes, blocs
Author: hatrix

La détection des lignes marchait bien jusque là. Et pourquoi jusque là ? Parce
que je ne l'avais testée que sur des exemples assez précis, n'excluant donc pas
certaines possibilités. Rappelez vous, il est très important de Test.er.

Donc le problème en soi : les points sur les i. Ou sur les j, ou les accents,
vous savez, ces caractères séparés en deux qui font chier. Quand il n'y a pas
de majuscule dans la phrase ou de caractère assez haut (k, l, f, etc.), les
points ou accents sont détectés comme étant une ligne, pas cool !

Voici donc ce qu'on pouvait se taper avant le fix :

![Détection de lignes foireuse](../images/lignes_rate.png)

J'ai donc mis en place un seuil fixe. C'est un peu crade et ça ne marchera pas
sur certaines images avec une résolution faible, mais c'est déjà pas trop trop
mal en soi. S'il y a moins de 5 pixels entre la ligne courante et la ligne
suivante, alors le tout forme une ligne. C'est tout con. Le problème étant
juste que ça dépend de la hauteur de la ligne, et ça, ça change selon les
résolutions. À 5 ça fait l'affaire, on verra déjà plus tard.

Après ce petit fix, on obtietn donc cette image :

![Détection de lignes foireuse](../images/lignes_ok.png)

On voit que les lignes à accents et points sont bien détectées ! Je ne sais pas
encore pour le moment quel technique utiliser pour gérer le seuil... Vu que les
lignes sont traitées à la suite et non à la fin, je ne peux pas faire, par
exemple, de moyenne sur la hauteur des lignes et ensuite en déduire quelque
chose. M'enfin, si ça foire vraiment trop, changer d'algo sera inévitable et
c'est pas plus mal, si ça marche.
