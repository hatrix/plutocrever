# -*- coding: UTF-8 -*-

SITENAME = 'PlutOCRever.'

DEFAULT_CATEGORY = 'News'

USE_CUSTOM_MENU = True
PDF_GENERATOR = False
REVERSE_CATEGORY_ORDER = True
DEFAULT_PAGINATION = 10
DEFAULT_LANG = 'french'

THEME = "zurb-F5-basic"

SITEURL="http://ocr.hatrix.fr/"

SOCIAL = (('bitbucket', 'https://bitbucket.org/Hatrix/plutocrever'),)

CUSTOM_MENUITEMS = (('Contact', 'pages/contact.html'),)

OUTPUT_PATH = './output'
