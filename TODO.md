## Liste des tâches

### Général
* [X] Makefile
* [X] Gestion des arguments
* [X] Site Web (pelican)

### Image
* [X] getPixel
* [X] setPixel
* [X] GrayScale
* [X] Rotation
* [X] Angle rotation
* [X] Bruit (filtre gaussien)
* [X] Binarisation

### Traitement
* [X] Extraction des lignes
* [X] Extraction des caractères
* [ ] Réseau de neurones

### GUI (GTK ?)

* [ ] LoadImageButton
* [ ] RotateImageButton
* [ ] GrayScaleButton
* [ ] FilterButton
* [ ] BinarizeButton
* [ ] ConvertButton
* [ ] CopyTextToClipboardButton
* [ ] Sauvegarde du texte dans un fichier
* [ ] Affichage de l'image avant/après traitement

### Bonus
* [ ] Détection de mise en forme (gras, souligné, etc.)
* [ ] Sauvegarde format word, latex, etc.
* [ ] Interface web (Flask ?)
* [ ] Correcteur orthographique

### Améliorations
* [X] Seuil d'extraction des lignes (points sur les i)
* [ ] Optimisation algorithme de Sauvola
* [ ] free des surfaces et autres tableaux (taille)
* [ ] Angle de rotation par dixième de degré

## Soutenance 1
* [X] Chargement d'une image
* [X] Détection et découpage des lignes, caractères
* [ ] Première version du réseau de neurones
* [X] Site web

## Soutenance 2
* [ ] Reconnaissance (découpage et réseau)
* [ ] Apprentissage avec sauvegarde et chargement
* [ ] Interface graphique avec sauvegarde au format texte
* [X] Niveaux de gris
* [X] Binarisation (noir et blanc)
* [X] Élimination du bruit
* [X] Détection de l'angle de rotation

