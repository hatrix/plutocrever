#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <stdlib.h>
#include <math.h>
#include "image.h"
#include "histogram.h"

#define max(a,b) (((a)>(b))?(a):(b))

Uint32 getPixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8*)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
        case 1:
            return *p;
            break;

        case 2:
            return *(Uint16*)p;
            break;

        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;
            break;

        case 4:
            return *(Uint32*)p;
            break;

        default:
            return 0;
    }
}

void setPixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8*)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
        case 1:
            *p = pixel;
            break;

        case 2:
            *(Uint16 *)p = pixel;
            break;

        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            } 
            else {
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;

        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}

void lockSurface(SDL_Surface *surface)
{
    if (SDL_MUSTLOCK(surface))
        SDL_LockSurface(surface);
}

void unlockSurface(SDL_Surface *surface)
{
    if (SDL_MUSTLOCK(surface))
        SDL_UnlockSurface(surface);
}

void grayScale(SDL_Surface *surface)
{
    lockSurface(surface);

    Uint8 r,g,b;
    Uint32 pixel;
    Uint32 color;

    for(int x=0; x < surface->w; x++)
    {
        for(int y=0; y < surface->h; y++)
        {
            // get pixel en rgb
            pixel = getPixel(surface, x, y);
            SDL_GetRGB(pixel, surface->format, &r, &g, &b);
            
            // passage en niveau de gris, getpixel correspondant au format
            r = r * 0.3 + g * 0.59 + b * 0.11;
            color = SDL_MapRGB(surface->format, r, r, r);

            // set pixel
            setPixel(surface, x, y, color);
        }
    }

    unlockSurface(surface);
}

SDL_Surface *binarizeSauvola(SDL_Surface *surface)
{
    lockSurface(surface);

    Uint8 r;
    Uint32 pixel;
    Uint32 color;
    int deviation = 0;
    int average = 0;
    int nb = 0;
    
    SDL_Surface *image_clean = createSurface(surface->w, surface->h);
    lockSurface(image_clean);

    int radius = surface->w < 40 ? surface->w : max(15, surface->w/40);
    int integ[surface->w][surface->h];

    for(int x=0; x < surface->w; x++)
    {
        for(int y=0; y < surface->h; y++)
        {
            pixel = getPixel(surface, x, y);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);

            integ[x][y] = r;

            if(x-1 >= 0 && y-1 >= 0){
                integ[x][y] = integ[x][y] + integ[x-1][y] + integ[x][y-1] 
                              - integ[x-1][y-1];
            }
            else if(x-1 >= 0){
                integ[x][y] = integ[x][y] + integ[x-1][y];
            }
            else if(y-1 >= 0){
                integ[x][y] = integ[x][y] + integ[x][y-1];
            }
        }
    }

    int x1, y1, x2, y2;
    for(int x=0; x < surface->w; x++)
    {
        for(int y=0; y < surface->h; y++)
        {
            average = 0;
            nb = 0;

            x1 = x - radius/2;
            y1 = y - radius/2;
            x2 = x + radius/2;
            y2 = y + radius/2;

            x1 = x1 < 0 ? 0 : x1;
            y1 = y1 < 0 ? 0 : y1;
            x2 = x2 > surface->w ? surface->w : x2;
            y2 = y2 > surface->h ? surface->h : y2;

            average = (integ[x2][y2] + integ[x1][y1] - integ[x1][y2] 
                      - integ[x2][y1]) / ((x2-x1) * (y2-y1));
            for(int xx=x-radius/2; xx < x+radius/2; xx++)
            {
                for(int yy=y-radius/2; yy < y+radius/2; yy++)
                {
                    if(xx >= 0 && xx < surface->w && yy > 0 && yy < surface->h)
                    {

                        pixel = getPixel(surface, xx, yy);
                        SDL_GetRGB(pixel, surface->format, &r, &r, &r);

                        deviation = deviation + pow(r - average, 2);
                        nb++;
                    }
                }
            }

            deviation = deviation / nb;
           
            pixel = getPixel(surface, x, y);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);

            average = average * (1 + 0.2 * (sqrt(deviation)/128 - 1));
            average = r > average ? 255 : 0;

            color = SDL_MapRGB(image_clean->format, average, average, 
                                                             average);
            setPixel(image_clean, x, y, color);
        }
    }
    
    unlockSurface(surface);
    unlockSurface(image_clean);
    
    return image_clean;
}

int mean(int *histo)
{
    int m = 0;
    for(int i = 0; i < 256; ++i)
        m += histo[i] * i;

    return m;
}

int threshold_image(SDL_Surface *surface)
{
    lockSurface(surface);
    
    int *histo = calloc(256, sizeof(int));
    grayHistogram(surface, histo);
    float average = mean(histo) / (surface->w * surface->h);

    printf("average: %f\n", average);

    int threshold;
    float deviation = 0;
    
    Uint8 r;
    Uint32 pixel;
    for(int y = 0; y < surface->h; y++)
    {
        for(int x = 0; x < surface->w; x++)
        {
            pixel = getPixel(surface, x, y);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            deviation += (r - average) * (r - average);
        }
    }
    float s = sqrt((1 / (surface->w * surface->h) * deviation));
    
    threshold = average * (1 - 0.2 * (1 - s / 128));
    
    unlockSurface(surface);
    return threshold;
}

void binarizeFixed(SDL_Surface *surface)
{
    lockSurface(surface);

    Uint8 r;
    Uint32 pixel;
    Uint32 color;

    int threshold = threshold_image(surface);

    for(int x = 0; x < surface->w; x++)
    {
        for(int y = 0; y < surface->h; y++)
        {
            pixel = getPixel(surface, x, y);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
           
            if(r > threshold)
                color = SDL_MapRGB(surface->format, 255, 255, 255);
            else
                color = SDL_MapRGB(surface->format, 0, 0, 0);

            setPixel(surface, x, y, color);
        }
    }

    unlockSurface(surface);
}

SDL_Surface *createSurface(int width, int height)
{
    Uint32 rmask, gmask, bmask, amask;
    if (SDL_BYTEORDER == SDL_BIG_ENDIAN){
        rmask = 0xff000000;
        gmask = 0x00ff0000;
        bmask = 0x0000ff00;
        amask = 0xff0000ff;
    }
    else{
        amask = 0xff000000;
        bmask = 0x00ff0000;
        gmask = 0x0000ff00;
        rmask = 0xff0000ff;
    }
    SDL_Surface *image_clean = SDL_CreateRGBSurface(0, width, height, 32, 
                                                  rmask, gmask, bmask, amask);
    return image_clean;
}

SDL_Surface *filter(SDL_Surface *surface) // filtre gaussien 3x3
{
    lockSurface(surface);

    Uint8 r;
    Uint32 pixel;
    Uint32 color;
    int gauss = 0;
    
    SDL_Surface *image_clean = createSurface(surface->w, surface->h);
    SDL_FillRect(image_clean, NULL, 
                 SDL_MapRGB(image_clean->format, 255, 255, 255));
    lockSurface(image_clean);

    for(int x = 1; x < surface->w-1; x++)
    {
        for(int y = 1; y < surface->h-1; y++)
        {
            pixel = getPixel(surface, x-1, y-1);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            gauss = r * 1;

            pixel = getPixel(surface, x, y-1);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            gauss = gauss + r * 2;
            
            pixel = getPixel(surface, x+1, y-1);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            gauss = gauss + r * 1;
            
            pixel = getPixel(surface, x-1, y);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            gauss = gauss + r * 2;
            
            pixel = getPixel(surface, x, y);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            gauss = gauss + r * 4;
            
            pixel = getPixel(surface, x+1, y);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            gauss = gauss + r * 2;
            
            pixel = getPixel(surface, x-1, y+1);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            gauss = gauss + r * 1;
        
            pixel = getPixel(surface, x, y+1);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            gauss = gauss + r * 2;
            
            pixel = getPixel(surface, x+1, y+1);
            SDL_GetRGB(pixel, surface->format, &r, &r, &r);
            gauss = gauss + r * 1;

            gauss = gauss / 16;
            color = SDL_MapRGB(image_clean->format, gauss, gauss, gauss);
            setPixel(image_clean, x, y, color);
        }
    }
    
    unlockSurface(surface);
    unlockSurface(image_clean);

    return image_clean;
}
