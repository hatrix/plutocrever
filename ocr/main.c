#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include "savepng/savepng.h"
#include "image.h"
#include "blocs.h"
#include "rotate.h"
#include "Network/net.h"
#include "Network/character.h"
#include "matrix.h"
#include "Network/character.h"

static void usage(int option)
{
    if(option == 0)
        printf("Usage: ./plutOCRever [-bfg] [-a | -B] [-r angle] "
               "image [-o output]\n");
    else
        printf("File does not exist\n");
    exit(1);
}

int main(int argc, char* argv[])
{
    char* img;
    char* output = "image.png";
    int angle = 0;      

    int aflag = 0;      // all
    int bflag = 0;      // binarisation de sauvola
    int fflag = 0;      // filtre gaussien
    int bfflag = 0;     // binarisation à seuil fixe
    int gflag = 0;      // grayscale
    int errflg = 0;     // error
    int rflag = 0;      // rotate
    int findflag = 0;   // find angle
    int farflag = 0;    // find and rotate
    int blockflag = 0;  // draw blocks
    int xorflag = 0;    // xor neural network
    int Xorflag = 0;    // xor neural network read
    int resizeflag = 0;  // resize matrix, test
    int c;

    struct option long_opt[] =
    {
        {"help",        no_argument,        NULL, 'h'},
        {"binarize",    no_argument,        NULL, 'b'},
        {"grayscale",   no_argument,        NULL, 'g'},
        {"fbinarize",   no_argument,        NULL, 'B'},
        {"filter",      no_argument,        NULL, 'f'},
        {"find-angle",  no_argument,        NULL, 'F'},
        {"find-rotate", no_argument,        NULL, 't'},
        {"blocks",      no_argument,        NULL,  0 },
        {"xor",         no_argument,        NULL, 'x'},
        {"Xor",         no_argument,        NULL, 'X'},
        {"resize",      no_argument,        NULL, 'R'},
        {"output",      required_argument,  NULL, 'o'},
        {"all",         required_argument,  NULL, 'a'},
        { NULL, 0, 0, 0}
    };

    while ((c = getopt_long(argc, argv, "abBfgho:r:FtclxX0R", long_opt, NULL)) 
            != -1)
    {
        switch (c)
        {
            case 'a':
                aflag = 1;
                break;
            case 'b':
                bflag = 1;
                break;
            case 'f':
                fflag = 1;
                break;
            case 'B':
                bfflag = 1;
                break;
            case 'g':
                gflag = 1;
                break;
            case 'h':
                usage(0);
            case 'o':
                output = optarg;
                break;
            case 'r':
                rflag = 1;
                angle = atoi(optarg);
                break;
            case 'F':
                findflag = 1;
                break;
            case 't':
                farflag = 1;
                break;
            case 'x':
                xorflag = 1;
                break;
            case 'X':
                Xorflag = 1;
                xorflag = 1;
                break;
            case 'R':
                resizeflag = 1;
                break;
            case 0:
                blockflag = 1;
                break;
            case '?':
                errflg++;
                break;
            default:
                usage(0);
        }
    }
    if(errflg || argc == optind)
        usage(0);
    if(optind < argc)
        img = argv[optind];
    if(access(img, F_OK) == -1)
        usage(1);

    SDL_Surface *image = IMG_Load(img);
    
    if(gflag || aflag)
        grayScale(image);
    if(fflag || aflag)
        image = filter(image);
    if(bfflag)
        binarizeFixed(image);
    if(bflag || aflag)
        image = binarizeSauvola(image);
    if(rflag)
        image = rotate(image, angle);
    if(findflag)
        printf("Angle optimal pour rotation : %d\n", findAngle(image));
    if(farflag)
        image = rotate(image, findAngle(image));


    if(blockflag)
    {
        vector *chars = detectBlocks(image);

        if(resizeflag)
        {
            vector_array *r_chars = create_vect_array();
            for(int i = 0; i < chars->used; i++)
            {
                double *tmp = rectToArray(image, &chars->data[i]);
                tmp = resizePixels(tmp, chars->data[i].w,
                                        chars->data[i].h, 16, 16);
                add_vector_array(r_chars, tmp);
            }

            for(int i = 0; i < r_chars->used; i++)
            {
                displayChar(r_chars->data[i], 16);
                printf("\n------------------------------\n");
            }
        }
        
        drawBlocks(image, chars);
    }
    
    if(xorflag)
    {
        int *layerSizes = (int *)malloc(3 * sizeof (int));
        layerSizes[0] = 256; layerSizes[1] = 512; layerSizes[2] = 35;

        /*Function *tFuncs = (Function *)malloc(3 * sizeof (Function));
        tFuncs[0] = None; tFuncs[1] = Sigmoid; tFuncs[2] = Sigmoid;*/

		setup(layerSizes, 3);

		vector *chars = detectBlocks(image);
		vector_array *r_chars = create_vect_array();
		for(int i=0; i < chars->used; i++)
		{
			double *tmp = rectToArray(image, &chars->data[i]);
			tmp = resizePixels(tmp, chars->data[i].w,
									chars->data[i].h, 16, 16);
			add_vector_array(r_chars, tmp);
		}

		/*double *truc = malloc(3, sizeof(double));
		for(int i=0; i< r_chars->used; i++)
		{
			truc = character(i, 3);
			for(int j=0; j<3; j++)
				printf("%f\n", truc[i]);
		}*/

        if(!Xorflag)
        {
            //Train the netork
            int max_count = 20000, count =0; double error = 0.0;

            do
            {
                count++;      
				error = 0.0;

                for(int i = 0; i < r_chars->used; i++)
                {
                    error += train(r_chars->data[i], 256, character(i), 35, 1, 0.15);
                }

            }while(error > 0.2 && count <= max_count);

            writeFile();
        }
        else
            readFile();

        //Display Results
        double *networkOutput = malloc(35 * sizeof (double));

		printf("%d\n", r_chars->used);

		FILE *fichier = NULL;
		fichier = fopen("Result.txt", "w+");
		fclose(fichier);

        for(int i = 0; i < r_chars->used; i++)
        {
            run(r_chars->data[i], networkOutput);
			printCharacter(networkOutput);
			/*if(i==14)         
			{
				for(int j=0; j<35; j++)
					printf("%f ", networkOutput[j]);
			}*/
		}

		printf("\n");
		//character(20);
		/*double *truc = calloc(94, sizeof (double));
		truc[0] = 1.0;
		printCharacter(truc);*/

    }

    if(!(aflag || bflag || fflag || gflag || bfflag || rflag || farflag
         || blockflag || xorflag))
        printf("Your didn't set any option, the image won't be modified.\n");
	if(!xorflag)
	{
    	SDL_SavePNG(image, output);
   		printf("Saving image to %s\n", output);
	}

    return 0;
}
