#include "vector.h"

int findAngle(SDL_Surface *surface);

SDL_Rect *getLines(SDL_Surface *surface, int *nb, SDL_Rect *bounds);

SDL_Rect *getChars(SDL_Surface *surface, SDL_Rect *lines, int length, int *nb);
void drawChars(SDL_Surface *surface, SDL_Rect *chars, int length);
void trimChars(SDL_Surface *surface, SDL_Rect *chars, int length);

vector *detectBlocks(SDL_Surface *surface);
void drawBlocks(SDL_Surface *surface, vector *vect);
