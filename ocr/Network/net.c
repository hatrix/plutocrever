# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <time.h>
# include "Gaussian.h"
# include "net.h"

# define TAILLE_MAX 1000

network *net;

double sigmoid(double x)
{   
    return 1.0/ (1.0 + exp(-x));
}

double sigmoidDerivative(double x)
{
    return sigmoid(x) * (1 - sigmoid(x));
}

void setup(int *layerSizes, int layerSizesLength)
{
    net = (network *)malloc(1 * sizeof (network));

    net->nbLayer = layerSizesLength - 1;
    net->inputSize = layerSizes[0];

    net->layerSize = (int *)malloc(net->nbLayer * sizeof (int));
    for(ssize_t i=0; i<net->nbLayer; i++)
        net->layerSize[i] = layerSizes[i+1];

    net->neuronOutput = (double **)malloc(net->nbLayer * sizeof (double *));
    net->neuronInput = (double **)malloc(net->nbLayer * sizeof (double *));
    net->bias = (double **)malloc(net->nbLayer * sizeof (double *));
    net->previousBias = malloc(net->nbLayer * sizeof (double *));
    net->error = (double **)malloc(net->nbLayer * sizeof (double *));
    net->weight = (double ***)malloc(net->nbLayer * sizeof (double **));
    net->previousWeight = malloc(net->nbLayer * sizeof (double **));

    for(ssize_t l=0; l<net->nbLayer; l++)
    {
        net->neuronOutput[l] = (double *)malloc(net->layerSize[l]
                                 * sizeof (double));
        net->neuronInput[l] = (double *)malloc(net->layerSize[l]
                                * sizeof (double));
        net->bias[l] = (double *)malloc(net->layerSize[l] * sizeof (double));
        net->previousBias[l] = malloc(net->layerSize[l] * sizeof (double));
        net->error[l] = (double *)malloc(net->layerSize[l] * sizeof (double));
        net->weight[l] = (double **)malloc((l==0? net->inputSize :
                           net->layerSize[l-1]) * sizeof (double *));
        net->previousWeight[l] = malloc((l==0? net->inputSize :
                            net->layerSize[l-1]) * sizeof (double *));

        for(int n=0; n<(l==0? net->inputSize : net->layerSize[l-1]); n++)
        {
            net->weight[l][n] = (double *)malloc(net->layerSize[l]
                                  * sizeof (double));
            net->previousWeight[l][n] = malloc(net->layerSize[l] * sizeof
                                    (double));
        }
    }

    srand(time(NULL));
    for(ssize_t l=0; l<net->nbLayer; l++)
    {
        for(int n=0; n<net->layerSize[l]; n++)
        {
            net->neuronOutput[l][n] = 0.0;
            net->neuronInput[l][n] = 0.0;
            net->error[l][n] = 0.0;
            net->previousBias[l][n] = 0.0;
            net->bias[l][n] = ((double)rand())/RAND_MAX;
        }

        for(int n=0; n<(l==0? net->inputSize :
             net->layerSize[l-1]); n++)
        {
            for(int s=0; s<net->layerSize[l]; s++)
            {   
                net->weight[l][n][s] = ((double)rand())/RAND_MAX;
                net->previousWeight[l][n][s] = 0.0;
                //printf("%f\n", net->weight[l][n][s]);
            }
        }
    }
}

void run(double *input, double *output)
{   
    for(ssize_t l=0; l<net->nbLayer; l++)
    {
        for(int n=0; n<net->layerSize[l]; n++)
        {
            double sum = 0.0;
            for(int s=0; s<(l==0? net->inputSize :
                 net->layerSize[l-1]); s++)
            {
                sum += net->weight[l][s][n] * (l==0? input[s] : 
                    net->neuronOutput[l-1][s]);
            }
            sum += net->bias[l][n];
        	net->neuronInput[l][n] = sum;

            net->neuronOutput[l][n] = sigmoid(sum);
        }
    }

    for(int i=0; i<net->layerSize[net->nbLayer-1]; i++)
        output[i] = net->neuronOutput[net->nbLayer-1][i];

}

double train(double *input, int inputLength, double *result, int resultLength,
                double learningRate, double Momemtum)
{
    if(inputLength != net->inputSize)
        printf("Invalid Input parameter");
    if(resultLength != net->layerSize[net->nbLayer-1])
        printf("Invalid result parameter");

    double sum = 0.0; double error = 0.0; double weightDelta = 0.0;
    double *output = (double *)malloc(net->layerSize[net->nbLayer-1]
                       * sizeof (double));

    run(input, output);

    for(ssize_t l = net->nbLayer-1; l>=0; l--)
    {
        if(l == net->nbLayer-1)
        {
            for(int n=0; n<net->layerSize[l]; n++)
            {
                net->error[l][n] = result[n] - output[n];
				error += 0.5 * (result[n] - output[n]) * (result[n] - output[n]);
            }
        }
        else
        {
            for(int n=0; n<net->layerSize[l]; n++)
            {
                sum = 0.0;
                for(int s=0; s<net->layerSize[l+1]; s++)
                {
                    sum += net->weight[l+1][n][s] * net->error[l+1][s];
                }
                net->error[l][n] = sum;
            }
        }
    }

    for(ssize_t l=0; l<net->nbLayer; l++)
        for(int n=0; n<(l==0? net->inputSize : net->layerSize[l-1]); n++)
            for(int s=0; s<net->layerSize[l]; s++)
            {
				weightDelta = learningRate * net->error[l][s]
                    * sigmoidDerivative(net->neuronInput[l][s]) * 
                    (l==0? input[n] : net->neuronOutput[l-1][n])
                    + Momemtum * net->previousWeight[l][n][s];

                net->weight[l][n][s] += weightDelta; 

                net->previousWeight[l][n][s] = weightDelta; 
            }

    for(ssize_t l=0; l<net->nbLayer; l++)
        for(int n=0; n<net->layerSize[l]; n++)
        {
            net->bias[l][n] += learningRate * net->error[l][n]
                    + Momemtum * net->previousBias[l][n];

            net->previousBias[l][n] = learningRate * net->error[l][n];
        }

		return error;
}

void writeFile()
{
    FILE *fichier = NULL;

    fichier = fopen("XOR_weight.txt", "w+");

    if(fichier != NULL)
    {

        for(ssize_t l=0; l<net->nbLayer; l++)
            for(int n=0; n<(l==0? net->inputSize : net->layerSize[l-1]); n++)
                for(int s=0; s<net->layerSize[l]; s++)
                {
                    fprintf(fichier, "%f\n", net->weight[l][n][s]);
                }

        fclose(fichier);
    }
    else
        printf("Impossible d'ouvrir le fichier XOR_weight.txt");

    fichier = fopen("XOR_bias.txt", "w+");

    if(fichier != NULL)
    {
        for(ssize_t l=0; l<net->nbLayer; l++)
            for(int n=0; n<net->layerSize[l]; n++)
            {
                fprintf(fichier, "%f\n", net->bias[l][n]);
            }
        fclose(fichier);
    }
    else
        printf("Impossible d'ouvrir le fichier XOR_bias.txt");
}

void readFile()
{
    FILE *fichier_weight = NULL;
    FILE *fichier_bias = NULL;

    fichier_weight = fopen("XOR_weight.txt", "r");
    fichier_bias = fopen("XOR_bias.txt", "r");

    char chain[TAILLE_MAX] = "";

    if(fichier_weight != NULL && fichier_bias != NULL)
    {
        for(ssize_t l=0; l<net->nbLayer; l++)
            for(int n=0; n<(l==0? net->inputSize : net->layerSize[l-1]); n++)
                for(int s=0; s<net->layerSize[l]; s++)
                {
                    fgets(chain, TAILLE_MAX, fichier_weight);
                    net->weight[l][n][s] = atof(chain);
                }
        for(ssize_t l=0; l<net->nbLayer; l++)
            for(int n=0; n<net->layerSize[l]; n++)
            {
                fgets(chain, TAILLE_MAX, fichier_bias);
                net->bias[l][n] = atof(chain);
            }
        fclose(fichier_weight);
        fclose(fichier_bias);
    }
}
