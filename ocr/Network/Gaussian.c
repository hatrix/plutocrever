#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double GetRandomGaussian(double mean, double stddev)
{
    double u, v, s, t, random;
    //srand(time(0));

    do
    { 
        random = rand() / (double)RAND_MAX;
        u = 2.0 * random - 1.0;
        v = 2.0 * random - 1.0;
    }while(u*u + v*v > 1 || (u == 0 && v == 0));

    s = u*u + v*v;
    t = sqrt((-2.0 * log(s)) / s);

    return stddev * u * t + mean;
}
