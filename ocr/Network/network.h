#ifndef NETWORK_H
# define NETWORK_H

typedef enum Function Function;
enum Function
{
    None,
    Sigmoid,
    Linear
};

typedef struct Network Network;
struct Network
{  
    ssize_t nbLayer;                // Number of Layer

    int inputSize;                  // Number of Input

    int *layerSize;                 // Size of the different layers

    double **neuronOutput;          /* Table containing the data calculated */
                                    /* of each neuron                       */

    double **neuronInput;           /* Table containing the inputs for each */
                                    /*   neuron                             */

    double **bias;                  // Table containing the bias

    double **error;                 /* Table containing the error calculated*/
                                    /*   for each neuron                    */

    double **previousBias;          /* Previous table of error for Momentum */
                                    /* Back-Propagation Method              */

    double ***weight;               // Table of weights

    double ***previousWeight;       /* Previous Table of weights for        */
                                    /* Momentum Back-Propagation Method     */
};

double evaluate(Function tFunc, double x);
double evaluateDerivative(Function tFunc, double x);
double sigmoid(double x);
double sigmoidDerivative(double x);
double linear(double x);
double linearDerivative(double x);

void setup(int *layerSizes, Function *tFuncs, int layerSizesLength,
            int tFuncsLength);

void run(double *input, int inputLength, double *output);
double train(double *input, int inputLength, double *result, int resultLength,
                double trainingRate, double Momentum);

#endif
