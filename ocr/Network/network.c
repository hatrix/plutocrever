#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Gaussian.h"
#include "network.h"

static Function *transferFunction;
Network *net;

double evaluate(Function tFunc, double x)
{   
    switch(tFunc)
    {
        case None:
            return 0.0;
            break;
        case Sigmoid:
            return sigmoid(x);
            break;
        case Linear:
            return linear(x);
            break;
        default :
            return 0.0;
            break;
    }
}

double evaluateDerivative(Function tFunc, double x)
{
    switch(tFunc)
    {
        case None:
            return 0.0;
            break;
        case Sigmoid:
            return sigmoidDerivative(x);
            break;
        case Linear:
            return linearDerivative(x);
            break;
        default:
            return 0.0;
            break;
    }
}

// Function to calculate the outputs
double sigmoid(double x)
{   
    return 1.0 / (1.0 + exp(-x));
}

// Function for the Back-propagation Method
double sigmoidDerivative(double x)
{   
    return sigmoid(x) * (1 - sigmoid(x));
}

double linear(double x)
{
    return x;
}

double linearDerivative(double x)
{
    return 1.0 + 0*x;
}

// Initialization of all variables
void setup(int *layerSizes, Function *tFuncs, int layerSizesLength,
            int tFuncsLength)
{
    if(tFuncsLength != layerSizesLength || tFuncs[0] != None)
        printf("We cannot construct a network with these parameters");

    net = (Network *)malloc(1*sizeof (Network));

    // Initialize network layer
    net->nbLayer = layerSizesLength - 1;
    net->inputSize = layerSizes[0];

    net->layerSize = (int *)malloc(net->nbLayer * sizeof (int));
    for(ssize_t i=0; i < net->nbLayer; i++)
        net->layerSize[i] = layerSizes[i+1];

    transferFunction = (Function *)malloc(net->nbLayer * sizeof(Function));
    for(ssize_t i=0; i < net->nbLayer; i++)
        transferFunction[i] = tFuncs[i+1];

    // Dimensionning arrays
    net->bias = (double **)malloc(net->nbLayer * sizeof (double*));
    net->previousBias = (double **)malloc(net->nbLayer * sizeof (double*));
    net->error = (double **)malloc(net->nbLayer * sizeof (double *));
    net->neuronOutput = (double **)malloc(net->nbLayer * sizeof (double *));
    net->neuronInput = (double **)malloc(net->nbLayer * sizeof (double *));

    net->weight = (double ***)malloc(net->nbLayer * sizeof (double **));
    net->previousWeight = (double ***)malloc(net->nbLayer * sizeof (double**));

    // Fill the arrays
    for(ssize_t layer=0; layer < net->nbLayer; layer++)
    {   
        net->bias[layer] = (double *)malloc(net->layerSize[layer] 
                             * sizeof (double));
        net->previousBias[layer] = (double *)malloc(net->layerSize[layer]
                                     * sizeof (double));
        net->error[layer] = (double *)malloc(net->layerSize[layer]
                              * sizeof (double));
        net->neuronOutput[layer] = (double *)malloc(net->layerSize[layer]
                                     * sizeof (double));
        net->neuronInput[layer] = (double *)malloc(net->layerSize[layer]
                                    * sizeof (double));
        net->weight[layer] = (double **)malloc((layer==0? net->inputSize :
                           net->layerSize[layer-1])  * sizeof (double *));
        net->previousWeight[layer] = (double **)malloc((layer==0?
                net->inputSize : net->layerSize[layer-1]) * sizeof (double *));

        for(int neuron=0; neuron < (layer==0 ? net->inputSize :
                net->layerSize[layer - 1]); neuron++)
        {
            net->weight[layer][neuron] = (double *)malloc(net->layerSize[layer]
                                           * sizeof (double));
            net->previousWeight[layer][neuron] = (double *)malloc(
                                      net->layerSize[layer] * sizeof (double));
        }
    }

    // Initialization of weights and biases
    for(ssize_t layer=0; layer < net->nbLayer; layer++)
    {
        for(int neuron=0; neuron < net->layerSize[layer]; neuron++)
        {
            net->bias[layer][neuron] = GetRandomGaussian(0.0, 1.0);
            net->previousBias[layer][neuron] = 0.0;
            net->error[layer][neuron] = 0.0;
            net->neuronOutput[layer][neuron] = 0.0;
            net->neuronInput[layer][neuron] = 0.0;
        }

        for(int neuron=0; neuron<(layer==0? net->inputSize : 
             net->layerSize[layer-1]); neuron++)
        {
            for(int syn=0; syn < net->layerSize[layer]; syn++)
            {
                net->weight[layer][neuron][syn] = GetRandomGaussian(0.0, 1.0);
                net->previousWeight[layer][neuron][syn] = 0.0;
            }
        }
    }

    // Main
    double **input = (double **)malloc(4 * sizeof (double *));
    double **output = (double **)malloc(4 * sizeof (double *));

    for(int i=0; i<4; i++)
    {
        input[i] = (double *)malloc(2 * sizeof (double));
        output[i] = (double *)malloc(1 * sizeof (double));
    }

    input[0][0] = 0.0; input[0][1] = 0.0; output[0][0] = 0.0;
    input[1][0] = 1.0; input[1][1] = 0.0; output[1][0] = 1.0;
    input[2][0] = 0.0; input[2][1] = 1.0; output[2][0] = 1.0;
    input[3][0] = 1.0; input[3][1] = 1.0; output[3][0] = 0.0;

    double error = 0.0;
    int max_count = 3000; int count = 0;

    do
    {
        count++;
        error = 0.0;

        for(int i=0; i<4; i++)
        {
            error += train(input[i], 2, output[i], 1, 0.15, 0.10);
        }

        if(count % 250 == 0)
        {
            printf("Iteration %d ----> Error : %f\n", count, error);
        }
    }while(error > 0.00001 && count <= max_count);

    printf("\n");
    double *networkOutput = (double *)malloc(1 * sizeof (double));

    for(int i=0; i<4; i++)
    {
        run(input[i], 2, networkOutput);
        printf("%f\n", networkOutput[0]);
    }
}

// Run the network and store results in output
void run(double *input, int inputLength, double *output)
{   
    if(inputLength != net->inputSize)
        printf("Input data is not of the correct dimension !");

    // Run the network
    for(ssize_t layer=0; layer < net->nbLayer; layer++)
    {
        for(int neuron=0; neuron<net->layerSize[layer]; neuron++)
        {
            double sum = 0.0;
            for(int syn=0; syn < (layer==0? net->inputSize : 
                     net->layerSize[layer-1]); syn++)
            {
                sum += net->weight[layer][neuron][syn] * (layer==0 ?
                         input[syn] : net->neuronOutput[layer-1][syn]);
            }
            sum += net->bias[layer][neuron];
            net->neuronInput[layer][neuron] = sum;

            net->neuronOutput[layer][neuron] = evaluate(
                        transferFunction[layer], sum);
        }
    }

    // Copy the output to the output array
    for(int neuron=0; neuron<net->layerSize[net->nbLayer - 1]; neuron++)
        output[neuron] = net->neuronOutput[net->nbLayer - 1][neuron];
}

double train(double *input, int inputLength, double *result, int resultLength,
                double trainingRate, double Momentum)
{   
    // Parameters validation
    if(inputLength != net->inputSize)
        printf("Invalid input parameter");
    if(resultLength != net->layerSize[net->nbLayer-1])
        printf("Invalid result parameter");

    // Local variables
    double e = 0.0, sum = 0.0, weightChange = 0.0, biasChange = 0.0;
    double *output = (double *)malloc(net->layerSize[net->nbLayer-1]
                       * sizeof (double));

    // Run the network
    run(input, inputLength, output); // actual output in the ouput array

    // Back-Propagate
    for(ssize_t layer = net->nbLayer - 1; layer >= 0; layer--)
    {
        if(layer == net->nbLayer - 1) // Output Layer
        {
            for(int neuron=0; neuron<net->layerSize[layer]; neuron++)
            {
                net->error[layer][neuron] = result[neuron] - output[neuron];
                e += pow(net->error[layer][neuron], 2);

                //Error of each neuron of the ouput layer
                net->error[layer][neuron] *= evaluateDerivative(
                    transferFunction[layer], net->neuronOutput[layer][neuron]);
            }
        }
        else // Hidden layer
        {
            for(int neuron=0; neuron<net->layerSize[layer]; neuron++)
            {
                sum = 0.0;
                for(int syn=0; syn<net->layerSize[layer+1]; syn++)
                    sum += net->weight[layer+1][neuron][syn] 
                            * net->error[layer+1][syn];

                sum *= evaluateDerivative(transferFunction[layer],
                            net->neuronOutput[layer][neuron]);

                // Error of each neuron in Hidden layers
                net->error[layer][neuron] = sum;
            }
        }
    }

    //Update of the Weights
    for(ssize_t layer=0; layer<net->nbLayer; layer++)
        for(int neuron=0; neuron<(layer==0? net->inputSize :
                net->layerSize[layer-1]); neuron++)
            for(int syn=0; syn<net->layerSize[layer]; syn++)
            {
                weightChange = trainingRate * net->error[layer][syn]
                * (layer==0? input[neuron] : net->neuronOutput[layer-1][neuron])
                *Momentum * net->previousWeight[layer][neuron][syn];

                net->weight[layer][neuron][syn] += weightChange;

                net->previousWeight[layer][neuron][syn] = weightChange;
            }

    //Update of the Biases
    for(ssize_t layer=0; layer<net->nbLayer; layer++)
        for(int neuron=0; neuron<net->layerSize[layer]; neuron++)
        {
            biasChange = trainingRate * net->error[layer][neuron];

            net->bias[layer][neuron] += biasChange + Momentum
                        * net->previousBias[layer][neuron];

            net->previousBias[layer][neuron] = biasChange;
        }

        return e;

}
