typedef struct Network network;
struct Network
{
    ssize_t nbLayer;                // Number of Layer

    int inputSize;                  // Number of Input

    int *layerSize;                 // Size of the different layers

    double **neuronOutput;          /* Table containing the data calculated */
                                    /* for each neuron                      */

    double **neuronInput;           /* Table containing the inputs for each */
                                    /* neuron                               */

    double **bias;                  // Table containing the bias

    double **previousBias;

    double **error;                 /* Table containing the error calculated*/
                                    /* for each neuron                      */

    double ***weight;                // Table of weights

    double ***previousWeight;
};

double sigmoid(double x);
double sigmoidDerivative(double x);

void setup(int *layerSizes, int layerSizesLength);

void run(double *input, double *output);
double train(double *input, int inputLength, double *result, int resultLength,
                double learningRate, double Momemtum);
void writeFile();
void readFile();
