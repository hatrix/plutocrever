#include <stdio.h>
#include <stdio.h>
#include <unistd.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <gtk/gtk.h>
#include "savepng/savepng.h"
#include "image.h"
#include "blocs.h"
#include "rotate.h"
#include "Network/net.h"
#include "Network/character.h"
#include "matrix.h"

int is_check_box_checked(GtkWidget *widget)
{
  return GTK_TOGGLE_BUTTON(widget)->active;
}

void process(GtkWidget **checkBoxes, char *img, char *output)
{
	SDL_Surface *image = IMG_Load(img);
	
  if (is_check_box_checked(checkBoxes[2]))
	{
		grayScale(image);
	}
	if (is_check_box_checked(checkBoxes[1]))
		image = filter(image);

	if (is_check_box_checked(checkBoxes[3]))
		binarizeFixed(image);

	if (is_check_box_checked(checkBoxes[0]))
		image = binarizeSauvola(image);

	if (is_check_box_checked(checkBoxes[4]))
		printf("Angle optimal pour rotation: %d\n", findAngle(image));

	if (is_check_box_checked(checkBoxes[5]))
		image = rotate(image, findAngle(image));

	if (is_check_box_checked(checkBoxes[8]))
	{
		vector *chars = detectBlocks(image);

		drawBlocks(image, chars);
	}

	if (is_check_box_checked(checkBoxes[9]) || 
	    is_check_box_checked(checkBoxes[01]))
	{
		int *layerSizes = (int *)malloc(3 * sizeof(int));
		layerSizes[0] = 900;
		layerSizes[1] = 1800;
		layerSizes[2] = 26;

		setup(layerSizes, 3);

		vector *chars = detectBlocks(image);
		vector_array *r_chars = create_vect_array();

		for (int i = 0; i < chars->used; i++)
		{
			double *tmp = rectToArray(image, &chars->data[i]);
			tmp = resizePixels(tmp, chars->data[i].w, 
			                        chars->data[i].h, 30, 30);
			add_vector_array(r_chars, tmp);
			free(tmp);
		}

		if (!is_check_box_checked(checkBoxes[11]))
		{
			//Train the network
			int max_count = 100000, count = 0;
			double error = 0.0;

			do
			{
				count++;
				error = 0.0;
				for (int i = 0; i < 4; i++)
				{
					error += train(r_chars->data[i], r_chars->used, character(i),
					               26, 0.2, 0.15);
				}
			
			} while (error > 0.000001 && count <= max_count);

			writeFile();
		}
		else
			readFile();
		free(chars);
		free(r_chars);
	}

	if (!is_check_box_checked(checkBoxes[10]))
	{
		SDL_SavePNG(image, output);
		printf("Save image to %s\n", output);
		img = output;
	}

	SDL_FreeSurface(image);
}

void pre_process(GtkWidget **checkBoxes, char *img, char *output)
{
	int nb_flags = 0;
	for (int i = 0; i < 11; i++)
	{
		if (is_check_box_checked(checkBoxes[i]))
			nb_flags++;
	}

	if (!nb_flags)
	{
		printf("You did not check any boxes, the image will not be modified.\n");
		return;
	}

	if (img == NULL || output == NULL)
	{
		if (img == NULL)
		{
			printf("Open an image.\n");
		}
		if (output == NULL)
		{
			output = "image.png";
		}
		return;
	}
	process(checkBoxes, img, output);
}

