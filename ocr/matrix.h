#include <SDL/SDL.h>

int **imgToMatrix(SDL_Surface *surface);
int **andMatrix(int **matrix1, int **matrix2, int w, int h);
SDL_Surface *matrixToImg(int **matrix, int w, int h);
double *rectToArray(SDL_Surface *surface, SDL_Rect *bounds);
double *resizePixels(double *pixels, int w1, int h1, int w2, int h2);
void displayChar(double *pixels, int size);
