void vHistogram(SDL_Surface *surface, int *table);
void vHistogramBounds(SDL_Surface *surface, int *table, SDL_Rect *bounds);

void hHistogram(SDL_Surface *surface, int *table);
void hHistogramBounds(SDL_Surface *surface, int *table, SDL_Rect *bounds);

void rHistogram(SDL_Surface *surface, int *table, int angle);
void grayHistogram(SDL_Surface *surface, int *table);

int **hrlsa(int **matrix, int width, int height);
int **vrlsa(int **matrix, int width, int height);
