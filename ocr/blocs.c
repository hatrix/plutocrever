#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <math.h>
#include <time.h>
#include "savepng/savepng.h"
#include "image.h"
#include "blocs.h"
#include "histogram.h"
#include "matrix.h"

#define ANGLE 20
#define LOG(string, ...) printf(string, ##__VA_ARGS__)

vector *characters;
int iter = 0;

int maxArrayPos(float *a, int nb)
{
    float max = 0;
    int pos = 0;
    for(int i = 0; i < nb; i++)
    {
        if (a[i] > max)
        {
            max = a[i];
            pos = i;
        }
    }
    return pos;
}

float getVariance(int *array)
{
    float variance = 0;
    float average = 0;
    int nb = array[0];
    for(int i=1; i <= nb; i++)
    {
        average += array[i];
    }
    average /= nb;

    for(int i=1; i <= nb; i++)
    {
        variance += pow((array[i] - average), 2);
    }
    variance /= nb;
    
    return variance;
}

int findAngle(SDL_Surface *surface)
{
    int histo[surface->h + 1];
    float variance[ANGLE*2 + 1];

    int i = 0;
    for(int angle=-ANGLE; angle <= ANGLE; angle++)
    {
        rHistogram(surface, histo, angle);
        variance[i] = getVariance(histo);
       
        i++;
    }

    return -(-ANGLE + maxArrayPos(variance, ANGLE*2 + 1));
}

int maxSDL_Rect(SDL_Rect *histo, int length)
{
    int pos = 0;
    for(int i = 0; i < length; i++)
    {
        if(histo[i].w > histo[pos].w)
            pos = i;
    }

    return pos;
}

vector *detectBlocks(SDL_Surface *surface)
{
    characters = create_vect();
    int w = surface->w;
    int h = surface->h;
    
    int **matrix = imgToMatrix(surface);
    matrix = andMatrix(hrlsa(matrix, w, h), vrlsa(matrix, w, h), w, h);

    SDL_Surface *img = matrixToImg(matrix, w, h);
    SDL_SavePNG(img, "rlsa.png");
    
    int length = 0;
    SDL_Rect bounds = {.x = 0, .y = 0, .w = surface->w, .h = surface->h - 1};
    SDL_Rect *blocks = getChars(img, &bounds, 1, &length);

    for(int i = 0; i < length; i++)
    {
        // pour le show
        //add_vector(characters, &blocks[i]);

        int length2 = 0;
        SDL_Rect *lines = getLines(surface, &length2, &blocks[i]);
        
        // pour le show
        //for(int j = 0; j < length2; j++)
        //    add_vector(characters, &lines[j]);

        int length3 = 0;
        SDL_Rect *chars = getChars(surface, lines, length2, &length3);
        trimChars(surface, chars, length3);

        if(length3 == 0)
        {
            for(int j = 0; j < length2; j++)
                add_vector(characters, &lines[j]);
        }

        for(int k = 0; k < length3; k++)
            add_vector(characters, &chars[k]);
    }
    
    return characters;
}

SDL_Rect *getLines(SDL_Surface *surface, int *nb, SDL_Rect *bounds)
{
    int histo[surface->h];
    vHistogramBounds(surface, histo, bounds);

    SDL_Rect *lines = malloc(sizeof(SDL_Rect));
   
    int y = bounds->y;
    int seuil = 0;
    int done = 1;
    int started = 0;

    for(int line = bounds->y + 1; line - bounds->y < bounds->h - 1; line++)
    {
        if (histo[line] == 0)
            seuil++;

        if(histo[line] > 0 && histo[line-1] == 0 && !started)
        {
            if(seuil < 5 && !done)
            {
                *nb -= 1;
                done = 1;

                y = line - seuil;
            }
            else
            {
                lines = realloc(lines, sizeof(SDL_Rect) * (*nb+1));
                lines[*nb].h = 0;

                lines[*nb].x = bounds->x;
                lines[*nb].y = line - seuil;
                lines[*nb].w = bounds->w;
                
                y = line - seuil;
                started = 1;
            }

            seuil = 0;
        }
        if((histo[line] > 0 && histo[line+1] == 0) && started)
        {
            lines[*nb].h = bounds->y + line - y + 1;

            y = 0;
            *nb += 1;

            done = 0;
            started = 0;
        }
    }

    return lines;
}

void drawBlocks(SDL_Surface *surface, vector *vect)
{
    srand(time(NULL));
    Uint8 c[3][3] = {{255, 0, 0}, {0, 255, 0}, {0, 0, 255}};
    Uint32 color;
    int p;
    
    lockSurface(surface);
    int x, y, x1, y1; 

    for(int i = 0; i < vect->used; i++)
    {
        p = rand() % 3;
        p = 1;
        color = SDL_MapRGB(surface->format, c[p][0], c[p][1], c[p][2]);

        x = vect->data[i].x;
        y = vect->data[i].y;
        x1 = x + vect->data[i].w;
        y1 = y + vect->data[i].h;

        for(int j = x; j < x1; j++)
        {
            setPixel(surface, j, y, color);
            setPixel(surface, j, y1, color);
        }
        for(int j = y; j < y1; j++)
        {
            setPixel(surface, x, j, color);
            setPixel(surface, x1, j, color);
        }
    }
    unlockSurface(surface);
}

void resetArray(int *array, int length)
{
    for(int i=0; i < length; i++)
    {
        array[i] = 0;
    }
}

SDL_Rect *getChars(SDL_Surface *surface, SDL_Rect *lines, int length, int *nb)
{
    int *histo = calloc(surface->w, sizeof(int));
    SDL_Rect *chars = malloc(sizeof(SDL_Rect));
    Uint32 pixel;
    Uint8 g;
    
    for(int i = 0; i < length; i++)
    {
        // histogramme
        for(int x = lines[i].x; x - lines[i].x < lines[i].w; x++)
        {
            for(int y=lines[i].y; y - lines[i].y < lines[i].h; y++)
            {   
                pixel = getPixel(surface, x, y);
                SDL_GetRGB(pixel, surface->format, &g, &g, &g);
                
                if(g == 0)
                    histo[x] += 1;
            }
        }
        
        // détection des caractères 
        // dépassement de bornes ?
        for(int column = lines[i].x; column - lines[i].x < lines[i].w; 
            column++)
        {
            if(histo[column] > 0 && histo[column-1] == 0)
            {
                chars[*nb].x = column;
                chars[*nb].y = lines[i].y;
                chars[*nb].h = lines[i].h;

            }

            if(histo[column] > 0 && histo[column+1] == 0)
            {
                chars[*nb].w = column - chars[*nb].x;
                *nb += 1;
                chars = realloc(chars, sizeof(SDL_Rect)*(*nb+1));
            }
        }
        resetArray(histo, surface->w);
    }
   
    return chars;
}

void trimChars(SDL_Surface *surface, SDL_Rect *chars, int length)
{
    int *histo = calloc(surface->w, sizeof(int));
    Uint32 pixel;
    Uint8 g;
    int up;

    for(int i=0; i < length; i++)
    {
        for(int y = chars[i].y; y - chars[i].y < chars[i].h; y++)
        {
            for(int x = chars[i].x; x - chars[i].x < chars[i].w; x++)
            {
                pixel = getPixel(surface, x, y);
                SDL_GetRGB(pixel, surface->format, &g, &g, &g);
                
                if(g == 0)
                    histo[y-chars[i].y] += 1;
            }
        }
    
        int ori_y = chars[i].y;
        int ori_h = chars[i].h;
        up = 1;
        
        for(int line = ori_y + 1; line - ori_y < ori_h - 1; line++)
        {
            if((histo[line-ori_y] && !histo[line-ori_y-1] && up)
               || (histo[0] && up))
            {
                chars[i].h = chars[i].h - (line - chars[i].y);
                chars[i].y = line; 
                up = 0;
            }
            
            if(histo[line-ori_y] && !histo[line-ori_y+1] && !histo[ori_h-1])
            {
                chars[i].h = line - chars[i].y;
            }
        }
        resetArray(histo, surface->w);
    }
}
