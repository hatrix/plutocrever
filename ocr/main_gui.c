#include <stdlib.h>
#include <gtk/gtk.h>
#include "process.h"

#define UNUSED(x) (void)(x);

struct data_info
{
	GtkWidget **checkboxes;
	GtkWidget *window, *container;
	char *filepath, *output;
};

void OnDestroy(GtkWidget *pWidget, gpointer pData)
{
	/* Arret de la boucle evenementielle */
	UNUSED(pWidget);
	UNUSED(pData);
	gtk_main_quit();
}

GtkWidget* create_label(char *text)
{
	GtkWidget *label= gtk_label_new(text);
	return label;
}

char* open_file_dialog(GtkWidget *parent_window)
{
	char *filename = NULL;
	GtkWidget *dialog;
	dialog = gtk_file_chooser_dialog_new("Open File",
			(GtkWindow*)parent_window,
			GTK_FILE_CHOOSER_ACTION_OPEN,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
			NULL);

	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
	{
		filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
	}
	gtk_widget_destroy(dialog);
	return filename;
}

void open_file_callback(GtkWidget *widget, gpointer data)
{
	struct data_info *datainfo = (struct data_info*)data;
	char *filename = open_file_dialog(datainfo->window);
	datainfo->filepath = filename;
}

void fix_button_size(GtkWidget *button, GtkRequisition *size)
{
	gtk_widget_set_size_request(button, size->width, size->height); 
}

void show_image_callback(GtkWidget *button, gpointer data)
{
	struct data_info *datainfo = (struct data_info*)data;
  if (!datainfo->filepath)
		return;
	GtkWidget *image_container = gtk_image_new_from_file(datainfo->filepath);
	GtkWidget *sWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	g_signal_connect(G_OBJECT(sWindow), "destroy", G_CALLBACK(OnDestroy),
			NULL);
	gtk_window_set_title(GTK_WINDOW(sWindow), "PlutOCRever - Image");
	gtk_container_add(GTK_CONTAINER(sWindow), image_container);
	gtk_widget_show_all(sWindow);
	gtk_main();
}

void process_button_callback(GtkWidget *widget, gpointer data)
{
	struct data_info *box = (struct data_info*)data;
	pre_process(box->checkboxes, box->filepath, box->output);
}

GtkWidget** create_check_buttons(GtkWidget *fixedBoxes)
{
	/* Création de toutes les check_box nécessaires */
	GtkWidget *checkBoxBinS = gtk_check_button_new_with_label(
	                              "Binarize image: Sauvola");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxBinS, 30, 30);

	GtkWidget *checkBoxFilter = gtk_check_button_new_with_label("Filter image");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxFilter, 30, 60);
	
	GtkWidget *checkBoxGray = gtk_check_button_new_with_label("Grayscale image");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxGray, 30, 90);
	
	GtkWidget *checkBoxBinT = gtk_check_button_new_with_label(
	                              "Binarize image: fixed treshold");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxBinT, 30, 120);
	
	GtkWidget *checkBoxFindA = gtk_check_button_new_with_label(
	                              "Find angle of image");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxFindA, 30, 150);

	GtkWidget *checkBoxFindR = gtk_check_button_new_with_label(
	                              "Find angle and rotate image");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxFindR, 30, 180);

	GtkWidget *checkBoxChars = gtk_check_button_new_with_label(
	                              "Draw detected chars");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxChars, 30, 210);

	GtkWidget *checkBoxLines = gtk_check_button_new_with_label(
	                              "Draw detected lines");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxLines, 30, 240);
	
	GtkWidget *checkBoxBlocks = gtk_check_button_new_with_label(
	                              "Draw detected blocks");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxBlocks, 30, 270);

	GtkWidget *checkBoxTAS = gtk_check_button_new_with_label(
	                              "Train and save neural network");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxTAS, 30, 300);

	GtkWidget *checkBoxRWT = gtk_check_button_new_with_label(
	                              "Run neural network without train");
	gtk_fixed_put(GTK_FIXED(fixedBoxes), checkBoxRWT, 30, 330);

	GtkWidget **checkBoxes = malloc(11 * sizeof(GtkWidget*));
	checkBoxes[0] = checkBoxBinS;
	checkBoxes[1] = checkBoxFilter;
	checkBoxes[2] = checkBoxGray;
	checkBoxes[3] = checkBoxBinT;
	checkBoxes[4] = checkBoxFindA;
	checkBoxes[5] = checkBoxFindR;
	checkBoxes[6] = checkBoxChars;
	checkBoxes[7] = checkBoxLines;
	checkBoxes[8] = checkBoxBlocks;
	checkBoxes[9] = checkBoxTAS;
	checkBoxes[10] = checkBoxRWT;

	return checkBoxes;
}

int main(int argc, char **argv)
{
	/* Déclaration du widget */
	GtkWidget *pWindow;
	gtk_init(&argc,&argv);

	/* Création de la fenêtre */
	pWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	/* Connexion du signal "destroy" */
	g_signal_connect(G_OBJECT(pWindow), "destroy", G_CALLBACK(OnDestroy), 
			NULL);

	/* Titre */
	gtk_window_set_title(GTK_WINDOW(pWindow), "PlutOCRever"); 

	/* Ajout d'une hbox pour séparer verticalement */
	GtkWidget *vbox = gtk_hbox_new(GTK_ORIENTATION_VERTICAL, 1);
	gtk_container_add(GTK_CONTAINER(pWindow), vbox);

	/* Ajout d'un fixed pour les boutons */
	GtkWidget *fixedButtons = gtk_fixed_new();
	gtk_box_pack_start(GTK_BOX(vbox), fixedButtons, FALSE, FALSE, 0);
	gtk_widget_set_size_request(fixedButtons, 250, 0);

	/* Ajout d'un fixed pour les checkbox */
	GtkWidget *fixedBoxes = gtk_fixed_new();
	gtk_box_pack_end(GTK_BOX(vbox), fixedBoxes, FALSE, FALSE, 0);
	gtk_widget_set_size_request(fixedBoxes, 300, 370);

	/* Création et affichage du bouton buttonOpenFile */
	GtkWidget *buttonOpenFile = gtk_button_new_with_label("Open File");
	struct data_info *datainfo = malloc(sizeof(struct data_info));
	datainfo->window = pWindow;
	datainfo->container = vbox;
	datainfo->filepath = NULL;
	datainfo->output = "image.png";
	gtk_fixed_put(GTK_FIXED(fixedButtons), buttonOpenFile, 75, 100);
	g_signal_connect(G_OBJECT(buttonOpenFile), "clicked",
			G_CALLBACK (open_file_callback), (gpointer)datainfo);


	/* Inutile pour l'instant 
	GtkRequisition *buttonSize = malloc(sizeof(GtkRequisition));
	buttonSize->height = 40;
	buttonSize->width = 100;*/

	gtk_widget_set_size_request(buttonOpenFile, 100, 40);

	/* Création du bouton buttonShowImage */
	GtkWidget *buttonShowImage = gtk_button_new_with_label("Show Image");
	gtk_widget_set_size_request(buttonShowImage, 100, 40);
	gtk_fixed_put(GTK_FIXED(fixedButtons), buttonShowImage, 75, 150);
	g_signal_connect(G_OBJECT(buttonShowImage), "clicked", 
	                 G_CALLBACK(show_image_callback), 
			(gpointer)datainfo);

	GtkWidget *buttonProcessOCR = gtk_button_new_with_label("Process");
	gtk_widget_set_size_request(buttonProcessOCR, 100, 40);
	gtk_fixed_put(GTK_FIXED(fixedButtons), buttonProcessOCR, 75, 200);

	/* Création de l'ensemble des checkbox */
	GtkWidget **checkBoxes = create_check_buttons(fixedBoxes);
	
	datainfo->checkboxes = checkBoxes;

	g_signal_connect(G_OBJECT(buttonProcessOCR), "clicked",
	                 G_CALLBACK(process_button_callback),
									 (gpointer)datainfo);
	
	/* Affichage de la fenêtre */
	gtk_widget_show_all(pWindow);

	/* Demarrage de la boucle evenementielle */
	gtk_main();
	free(datainfo);

	return EXIT_SUCCESS;
}
