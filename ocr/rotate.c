#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <math.h>
#include "image.h"
#include "blocs.h"

SDL_Surface *rotate(SDL_Surface *image, int angle)
{
    /* Trigo */
    double c = cos(3.14159*angle/180);
    double s = sin(3.14159*angle/180);

    /* Original Dimensions */
    int w = image->w;
    int h = image->h;
    
    /* New dimensions */
    int wo = abs(w*c) + abs(h*s);
    int ho = abs(h*c) + abs(w*s);

    SDL_Surface *new_img;
    new_img = createSurface(wo, ho);

    SDL_FillRect(new_img, NULL, SDL_MapRGB(new_img->format, 255, 255, 255));

    /* Middle coordinates */
    int ocx = w / 2;
    int ocy = h / 2;

    /* New middle coordinates */
    int ncx = wo / 2;
    int ncy = ho / 2;

    Uint8 r, g, b;
    Uint32 pixel;
    Uint32 color;

    lockSurface(image);
    lockSurface(new_img);
    for (int y = 0; y < ho; y++)
    {
        for(int x = 0; x < wo; x++)
        {
            /* New coordinates */
            int xo = ocx + (c*(x - ncx) + s*(y - ncy));
            int yo = ocy + (c*(y - ncy) - s*(x - ncx));

            if (xo >= 0 && yo >= 0 && xo < image->w && yo < image->h)
            {
                pixel = getPixel(image, xo, yo);
                SDL_GetRGB(pixel,image->format,&r,&g,&b);
                color = SDL_MapRGB(new_img->format,r,g,b);
                setPixel(new_img,x,y,color);
            }
        }
    }
    unlockSurface(image);
    unlockSurface(new_img);

    return new_img;
}
