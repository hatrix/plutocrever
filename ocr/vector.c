#include <stdlib.h>
#include <stdio.h>
#include "vector.h"

vector *create_vect()
{
    vector *new = malloc(sizeof(vector));
    new->data = malloc(100 * sizeof(SDL_Rect));
    new->size = 100;
    new->used = 0;

    return new;
}

void add_vector(vector *vect, SDL_Rect *elm)
{
    if(vect->used + 1 >= vect->size)
    {
        vect->data = realloc(vect->data, sizeof(vector) * vect->size * 2);
        vect->size *= 2;
    }
    
    vect->data[vect->used] = *elm;
    vect->used += 1;
}

void destroy_vect(vector **vect)
{
    free((*vect)->data);
    free(*vect);
    *vect = NULL;
}


vector_array *create_vect_array()
{
    vector_array *new = malloc(sizeof(vector_array));
    new->data = malloc(100 * sizeof(double*));
    new->size = 100;
    new->used = 0;

    return new;
}

void add_vector_array(vector_array *vect, double *pixels)
{
    if(vect->used + 1 >= vect->size)
    {
        vect->data = realloc(vect->data, sizeof(double*) * vect->size * 2);
        vect->size *= 2;
    }
    
    vect->data[vect->used] = pixels;
    vect->used += 1;
}
