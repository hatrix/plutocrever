#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <math.h>
#include "image.h"

int **createMatrix(int width, int height)
{
    int **matrix = calloc(width, sizeof(int*));
    for(int i = 0; i < width; i++)
    {
        matrix[i] = calloc(height, sizeof(int));
    }

    return matrix;
}

int **imgToMatrix(SDL_Surface *surface)
{
    int **matrix = createMatrix(surface->w, surface->h);
    Uint32 pixel;
    Uint8 color;
    for(int x = 0; x < surface->w; x++)
    {
        for(int y = 0; y < surface->h; y++)
        {
            pixel = getPixel(surface, x, y);
            SDL_GetRGB(pixel, surface->format, &color, &color, &color);
            matrix[x][y] = color;
        }
    }

    return matrix;
}

int **andMatrix(int **matrix1, int **matrix2, int width, int height)
{
    int **result = createMatrix(width, height);
    for(int x = 0; x < width; x++)
    {
        for(int y = 0; y < height; y++)
        {
            result[x][y] = matrix1[x][y] & matrix2[x][y];
        }
    }

    return result;
}

SDL_Surface *matrixToImg(int **matrix, int width, int height)
{
    Uint32 color;
    Uint8 val;

    SDL_Surface *img = createSurface(width, height);
    lockSurface(img);
    SDL_FillRect(img, NULL, SDL_MapRGB(img->format, 255, 255, 255));

    for(int x = 0; x < width; x++)
    {
       for(int y = 0; y < height; y++)
        {
            val = matrix[x][y];
            color = SDL_MapRGB(img->format, val, val, val);
            setPixel(img, x, y, color);
        }
    }
    
    unlockSurface(img);
    return img;
}

double *rectToArray(SDL_Surface *surface, SDL_Rect *bounds)
{
    double *pixels = malloc(sizeof(double) * bounds->w * bounds->h);
    Uint32 pixel;
    Uint8 color;

    int c = 0;

    for(int y = bounds->y; y < bounds->y + bounds->h; y++)
    {
        for(int x = bounds->x; x < bounds->x + bounds->w; x++)
        {
            pixel = getPixel(surface, x, y);
            //printf("height: %d, y: %d\n", bounds.h + bounds.y, y);
            SDL_GetRGB(pixel, surface->format, &color, &color, &color);
            pixels[c] = color == 0 ? 1 : 0;
            c++;

            //if(color == 255)
            //    printf(" ");
            //else
            //    printf("X");

        }
        //printf("\n");
    }
    //printf("\n ------------------------------------------- \n");

    return pixels;

}

double *resizePixels(double *pixels, int w1, int h1, int w2, int h2)
{
    double *temp = malloc(sizeof(double) * w2 *h2);
    double x_ratio = w1 / (double)w2 ;
    double y_ratio = h1 / (double)h2 ;
    double px, py; 

    for (int i = 0; i < h2; i++)
    {
        for (int j = 0; j < w2; j++)
        {
            px = floor(j*x_ratio);
            py = floor(i*y_ratio);
            temp[(i*w2)+j] = pixels[(int)((py*w1)+px)];
        }
    }
    return temp;
}

void displayChar(double *pixels, int size)
{
    char shape;
    for(int i = 0; i < size * size; i++)
    {
        if(i % size == 0)
            printf("\n");
 
        if(pixels[i] == 1)
            shape = 'X';
        else
            shape = ' ';
        printf("%c", shape);
    }
    puts("");
}
