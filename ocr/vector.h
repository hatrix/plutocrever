#include <SDL/SDL.h>

typedef struct
{
    int size;
    int used;
    SDL_Rect *data;
}vector;

typedef struct
{
    int size;
    int used;
    double **data;
}vector_array;

vector *create_vect();
vector_array *create_vect_array();

void add_vector(vector *vect, SDL_Rect *elm);
void add_vector_array(vector_array *vect, double *pixels);

void destroy_vect(vector **vect);
